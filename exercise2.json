{
    "$schema": "https://schema.management.azure.com/schemas/2019-04-01/deploymentTemplate.json#",
    "contentVersion": "1.0.0.0",
    "parameters": {
      "appName": {
        "type": "string",
        "minLength": 1,
        "maxLength": 15,
        "metadata": {
          "description": "Use lower case number so this value can be used for items like the Storage account name."
        }
      },
      "locationShort": {
        "type": "string",
        "minLength": 2,
        "maxLength": 3,
        "metadata": {
          "description": "Two to three character value that identifies the region into which the resources are deployed. Should map to the resource group's location."
        }
      },
      "skuName": {
        "type": "string",
        "defaultValue": "S1",
        "allowedValues": [
          "S1",
          "S2",
          "S3",
          "P1",
          "P2",
          "P3",
          "P4"
        ],
        "metadata": {
          "description": "Describes plan's pricing tier and instance size. Check details at https://azure.microsoft.com/en-us/pricing/details/app-service/"
        }
      },
      "skuCapacity": {
        "type": "int",
        "defaultValue": 1,
        "minValue": 1,
        "metadata": {
          "description": "Describes plan's instance count"
        }
      },
      "administratorLogin": {
        "type": "string"
      },
      "administratorLoginPassword": {
        "type": "securestring"
      },
      "collation": {
        "type": "string",
        "defaultValue": "SQL_Latin1_General_CP1_CI_AS"
      },
      "edition": {
        "type": "string",
        "defaultValue": "Standard",
        "allowedValues": [
          "Standard",
          "Premium"
        ]
      },
      "maxSizeBytes": {
        "type": "string",
        "defaultValue": "1073741824"
      },
      "requestedServiceObjectiveName": {
        "type": "string",
        "defaultValue": "S0",
        "allowedValues": [
          "S0",
          "S1",
          "S2",
          "P1",
          "P2",
          "P3"
        ],
        "metadata": {
          "description": "Describes the performance level for Azure SQL Database Edition"
        }
      },
      "storageAccountType": {
        "type": "string",
        "defaultValue": "Standard_LRS",
        "allowedValues": [
          "Standard_LRS",
          "Standard_ZRS",
          "Standard_GRS",
          "Standard_RAGRS",
          "Premium_LRS"
        ]
      },
      "dbContainerName": {
        "type": "string"
      },
      "imagesContainerName": {
        "type": "string"
      },
      "leadImage": {
        "type": "string"
      },
      "workspaceSku": {
        "type": "String",
        "allowedValues": [
          "Standalone",
          "PerNode",
          "PerGB2018"
        ],
        "defaultValue": "PerGB2018",
        "metadata": {
          "description": "Specifies the service tier of the workspace: Standalone, PerNode, Per-GB"
        }
      },
      "actionGroupName": {
          "type": "string",
          "metadata": {
              "description": "Unique name (within the Resource Group) for the Action group."
          }
      },
      "actionGroupShortName": {
          "type": "string",
          "metadata": {
              "description": "Short name (maximum 12 characters) for the Action group."
          }
      },
      "emailName": {
          "type": "string",
          "metadata": {
              "description": "name of the alert email recipient"
          }
      },
      "emailAddress": {
          "type": "string",
          "metadata": {
              "description": "email address of the alert email recipient"
          }
      },
      "pingText": {
          "type": "string",
          "defaultValue": ""
      },
      "activityLogAlertsName": {
          "defaultValue": "ServiceHealthActivityLogAlert",
          "type": "String"
      },
      "ServiceHealthServices": {
          "type": "array"
      },
      "ServiceHealthRegions": {
          "type": "array"
      }
    },
    "variables": {
      "webSiteName": "[concat(parameters('appName'), '-web')]",
      "sqlServerName": "[concat(parameters('appName'), '-sql')]",
      "databaseName": "[concat(parameters('appName'), '-db')]",
      "hostingPlanName": "[concat(parameters('appName'), '-', parameters('locationShort'), '-web')]",
      "storageAccountName": "[concat(parameters('appName'), 'storage001')]",
      "logsStorageAccount": "[concat(parameters('appName'), 'storage002')]",
      // by default, workspaces are soft deleted when the delete action is taken, therefore name is reserved for 30 day afterwards.
      "workspaceName": "[concat(parameters('appName'), '-', parameters('locationShort'), '-web')]",
      "pingURL": "[concat('https://', variables('webSiteName'), '.azurewebsites.net')]",
      "autoscaleName": "[concat(variables('hostingPlanName'), '-autoscale-', resourceGroup().name)]",
      "pingTestName": "[concat(parameters('appName'), '-pingtest')]",
      "pingAlertRuleName": "[concat(parameters('appName'), '-pingalert')]",
      "alertScope":"[concat('/','subscriptions','/',subscription().subscriptionId)]",
      "appWelcomeTitle": "[concat('Welcome to ', parameters('appName'))]",
      // imageStorageUrl used as an env var in app
      "imageStorageUrl": "[concat('https://', variables('storageAccountName'), '.blob.core.windows.net/', parameters('imagesContainerName'), '/')]"
    },
    "resources": [
      {
        "name": "[variables('sqlServerName')]",
        "type": "Microsoft.Sql/servers",
        "location": "[resourceGroup().location]",
        "tags": {
          "displayName": "SqlServer"
        },
        "apiVersion": "2019-06-01-preview",
        "properties": {
          "administratorLogin": "[parameters('administratorLogin')]",
          "administratorLoginPassword": "[parameters('administratorLoginPassword')]"
        },
        "resources": [
          {
            "name": "[variables('databaseName')]",
            "type": "databases",
            "location": "[resourceGroup().location]",
            "tags": {
              "displayName": "Database"
            },
            "apiVersion": "2019-06-01-preview",
            "dependsOn": [
              "[concat('Microsoft.Sql/servers/', variables('sqlServerName'))]"
            ],
            "properties": {
              "edition": "[parameters('edition')]",
              "collation": "[parameters('collation')]",
              "maxSizeBytes": "[parameters('maxSizeBytes')]",
              "requestedServiceObjectiveName": "[parameters('requestedServiceObjectiveName')]"
            }
          },
          {
            "type": "firewallrules",
            "apiVersion": "2015-05-01-preview",
            "dependsOn": [
              "[concat('Microsoft.Sql/servers/', variables('sqlServerName'))]"
            ],
            "location": "[resourceGroup().location]",
            "name": "AllowAllWindowsAzureIps",
            "properties": {
              "endIpAddress": "0.0.0.0",
              "startIpAddress": "0.0.0.0"
            }
          }
        ]
      },
      {
        "apiVersion": "2018-02-01",
        "name": "[variables('hostingPlanName')]",
        "type": "Microsoft.Web/serverfarms",
        "location": "[resourceGroup().location]",
        "tags": {
          "displayName": "HostingPlan"
        },
        "sku": {
          "name": "[parameters('skuName')]",
          "capacity": "[parameters('skuCapacity')]"
        },
        "properties": {
          "name": "[variables('hostingPlanName')]"
        }
      },
      {
        "apiVersion": "2018-02-01",
        "name": "[variables('webSiteName')]",
        "type": "Microsoft.Web/sites",
        "location": "[resourceGroup().location]",
        "dependsOn": [
          "[concat('Microsoft.Web/serverFarms/', variables('hostingPlanName'))]",
          // let the app insights resource build first
          "[resourceId('microsoft.insights/components/', variables('webSiteName'))]"
        ],
        "tags": {
          "[concat('hidden-related:', resourceGroup().id, '/providers/Microsoft.Web/serverfarms/', variables('hostingPlanName'))]": "empty",
          "displayName": "Website"
        },
        "properties": {
          "name": "[variables('webSiteName')]",
          "serverFarmId": "[resourceId('Microsoft.Web/serverfarms', variables('hostingPlanName'))]",
          "httpsOnly": true,
          "siteConfig": {
            // use this area to enable the app insights and setup env vars for the application
              "appSettings": [
                  {
                      "name": "APPINSIGHTS_INSTRUMENTATIONKEY",
                      "value": "[reference(concat('microsoft.insights/components/', variables('webSiteName')), '2015-05-01').InstrumentationKey]"
                  },
                  {
                      "name": "APPLICATIONINSIGHTS_CONNECTION_STRING",
                      "value": "[reference(concat('microsoft.insights/components/', variables('webSiteName')), '2015-05-01').ConnectionString]"
                  },
                  {
                      "name": "ApplicationInsightsAgent_EXTENSION_VERSION",
                      "value": "~2"
                  },
                  {
                      "name": "DiagnosticServices_EXTENSION_VERSION",
                      "value": "~3"
                  },
                  {
                      "name": "XDT_MicrosoftApplicationInsights_Mode",
                      "value": "default"
                  },
                  {
                    "name": "dbServer",
                    "value": "[concat('tcp:', reference(variables('sqlServerName')).fullyQualifiedDomainName, ',1433')]"
                  },
                  {
                    "name": "dbName",
                    "value": "[variables('databaseName')]"
                  },
                  {
                    "name": "dbUser",
                    "value": "[parameters('administratorLogin')]"
                  },
                  {
                    "name": "dbPass",
                    "value": "[parameters('administratorLoginPassword')]"
                  },
                  {
                    "name": "leadImage",
                    "value": "[parameters('leadImage')]"
                  },
                  {
                    "name": "appWelcomeTitle",
                    "value": "[variables('appWelcomeTitle')]"
                  },
                  {
                    "name": "imageStorageUrl",
                    "value": "[variables('imageStorageUrl')]"
                  }
              ]
          }
        },
        "resources": [
          {
            "apiVersion": "2018-11-01",
            "type": "slots",
            "name": "Staging",
            "dependsOn": [
              "[concat('Microsoft.Web/Sites/', variables('webSiteName'))]"
            ],
            "location": "[resourceGroup().location]",
            "properties": {
            }
          },
          {
            "apiVersion": "2018-11-01",
            "type": "slots",
            "name": "LastKnownGood",
            "dependsOn": [
              "[concat('Microsoft.Web/Sites/', variables('webSiteName'))]"
            ],
            "location": "[resourceGroup().location]",
            "properties": {
            }
          },
        {
            "apiVersion": "2018-11-01",
            "name": "Microsoft.ApplicationInsights.AzureWebSites",
            "type": "siteextensions",
            "dependsOn": [
                "[resourceId('Microsoft.Web/Sites', variables('webSiteName'))]"
            ],
            "properties": {
            }
        }
        ]
      },
      //setup autoscaling
      {
        "apiVersion": "2014-04-01",
        "name": "[variables('autoscaleName')]",
        "type": "Microsoft.Insights/autoscalesettings",
        "location": "[resourceGroup().location]",
        "tags": {
          "[concat('hidden-link:', resourceGroup().id, '/providers/Microsoft.Web/serverfarms/', variables('hostingPlanName'))]": "Resource",
          "displayName": "AutoScaleSettings"
        },
        "dependsOn": [
          "[concat('Microsoft.Web/serverfarms/', variables('hostingPlanName'))]"
        ],
        "properties": {
          "profiles": [
            {
              "name": "Default",
              "capacity": {
                "minimum": 1,
                "maximum": 2,
                "default": 1
              },
              "rules": [
                {
                  "metricTrigger": {
                    "metricName": "CpuPercentage",
                    "metricResourceUri": "[concat(resourceGroup().id, '/providers/Microsoft.Web/serverfarms/', variables('hostingPlanName'))]",
                    "timeGrain": "PT1M",
                    "statistic": "Average",
                    "timeWindow": "PT10M",
                    "timeAggregation": "Average",
                    "operator": "GreaterThan",
                    "threshold": 80.0
                  },
                  "scaleAction": {
                    "direction": "Increase",
                    "type": "ChangeCount",
                    "value": 1,
                    "cooldown": "PT10M"
                  }
                },
                {
                  "metricTrigger": {
                    "metricName": "CpuPercentage",
                    "metricResourceUri": "[concat(resourceGroup().id, '/providers/Microsoft.Web/serverfarms/', variables('hostingPlanName'))]",
                    "timeGrain": "PT1M",
                    "statistic": "Average",
                    "timeWindow": "PT1H",
                    "timeAggregation": "Average",
                    "operator": "LessThan",
                    "threshold": 60.0
                  },
                  "scaleAction": {
                    "direction": "Decrease",
                    "type": "ChangeCount",
                    "value": 1,
                    "cooldown": "PT1H"
                  }
                }
              ]
            }
          ],
          "enabled": true,
          "name": "[variables('autoscaleName')]",
          "targetResourceUri": "[concat(resourceGroup().id, '/providers/Microsoft.Web/serverfarms/', variables('hostingPlanName'))]"
        }
      },
      {
        "type": "Microsoft.Insights/autoscalesettings/providers/diagnosticSettings",
        "apiVersion": "2017-05-01-preview",
        "name": "[concat(variables('autoscaleName'),'/microsoft.insights/diagSetting')]",
        "dependsOn": [
          "[resourceId('Microsoft.Insights/autoscalesettings', variables('autoscaleName'))]",
          "[resourceId('Microsoft.OperationalInsights/workspaces', variables('workspaceName'))]"
        ],
        "properties": {
          "workspaceId": "[resourceId('Microsoft.OperationalInsights/workspaces', variables('workspaceName'))]",
          "logs": [
            {
              "category": "AutoscaleScaleActions",
              "enabled": true
            },
            {
              "category": "AutoscaleEvaluations",
              "enabled": true
            }
          ]
        }
      },
            {
        "type": "Microsoft.Web/sites/providers/diagnosticSettings",
        "apiVersion": "2017-05-01-preview",
        "name": "[concat(variables('webSiteName'),'/microsoft.insights/diagSetting')]",
        "dependsOn": [
          "[resourceId('Microsoft.Web/sites', variables('webSiteName'))]",
          "[resourceId('Microsoft.OperationalInsights/workspaces', variables('workspaceName'))]",
          "[resourceId('Microsoft.Storage/storageAccounts', variables('logsStorageAccount'))]"

        ],
        "properties": {
          "workspaceId": "[resourceId('Microsoft.OperationalInsights/workspaces', variables('workspaceName'))]",
          "storageAccountId": "[resourceId('Microsoft.Storage/storageAccounts', variables('logsStorageAccount'))]",
          "logs": [
            {
              "category": "AppServiceHTTPLogs",
              "enabled": true,
              "retentionPolicy": {
                "enabled": true,
                "days": 2
              }
            },
            {
              "category": "AppServiceConsoleLogs",
              "enabled": true,
              "retentionPolicy": {
                "enabled": true,
                "days": 2
              }
            },
            {
              "category": "AppServiceAppLogs",
              "enabled": true,
              "retentionPolicy": {
                "enabled": true,
                "days": 2
              }
            },
            {
              "category": "AppServiceFileAuditLogs",
              "enabled": true,
              "retentionPolicy": {
                "enabled": true,
                "days": 2
              }
            },
            {
              "category": "AppServiceAuditLogs",
              "enabled": true,
              "retentionPolicy": {
                "enabled": true,
                "days": 2
              }
            }
          ],
          "metrics": [
            {
              "category": "AllMetrics",
              "enabled": true,
              "retentionPolicy": {
                "enabled": true,
                "days": 2
              }
            }
        ]   
        }
      },
      // create the app insights component
      {
          "apiVersion": "2015-05-01",
          "name": "[variables('webSiteName')]",
          "type": "Microsoft.Insights/components",
          "location": "[resourceGroup().location]",
          "tags": {
              "[concat('hidden-link:', resourceGroup().id, '/providers/Microsoft.Web/sites/', variables('webSiteName'))]": "Resource",
              "displayName": "AppInsightsComponent"
          },
          "kind": "web",
          "properties": {
            "Application_Type": "web",
            "ApplicationId": "[variables('webSiteName')]",
            "Request_Source": "IbizaWebAppExtensionCreate"
          }
      },
      // create a storage account for logs
      {
        "name": "[variables('logsStorageAccount')]",
        "type": "Microsoft.Storage/storageAccounts",
        "location": "[resourceGroup().location]",
        "apiVersion": "2019-06-01",
        "dependsOn": [ ],
        "tags": {
          "displayName": "StorageAccount"
        },
        "kind": "StorageV2",
        "sku": {
          "name": "[parameters('storageAccountType')]"
        },
        "properties": {}
      },
      // create the storage account and containers for app data and db backups
      {
        "name": "[variables('storageAccountName')]",
        "type": "Microsoft.Storage/storageAccounts",
        "location": "[resourceGroup().location]",
        "apiVersion": "2019-06-01",
        "dependsOn": [ ],
        "tags": {
          "displayName": "StorageAccount"
        },
        "kind": "StorageV2",
        "sku": {
          "name": "[parameters('storageAccountType')]"
        },
        "properties": {},
        "resources": [
          {
            "name": "[concat('default/', parameters('dbcontainerName'))]",
            "type": "blobServices/containers",
            "apiVersion": "2019-06-01",
            "dependsOn": [
                "[variables('storageAccountName')]"
            ],
            "properties": {
                "publicAccess": "None"
            }
          },
          {
            "name": "[concat('default/', parameters('imagescontainerName'))]",
            "type": "blobServices/containers",
            "apiVersion": "2019-06-01",
            "dependsOn": [
                "[variables('storageAccountName')]"
            ],
            "properties": {
                "publicAccess": "Blob"
            }
          }
        ]
      },
      {
        "type": "Microsoft.OperationalInsights/workspaces",
        "name": "[variables('workspaceName')]",
        "apiVersion": "2015-11-01-preview",
        "location": "[resourceGroup().location]",
        "properties": {
          "sku": {
            "Name": "[parameters('workspaceSku')]"
          },
          "features": {
            "searchVersion": 1
          }
        }
      },
      {
      "name": "[variables('pingTestName')]",
      "type": "Microsoft.Insights/webtests",
      "apiVersion": "2014-04-01",
      "location": "[resourceGroup().location]",
      "dependsOn": [
        "[resourceId('Microsoft.Insights/actionGroups', parameters('actionGroupName'))]",
        "[resourceId('microsoft.insights/components/', variables('webSiteName'))]"
      ],
      "tags": {
        "[concat('hidden-link:', resourceId('Microsoft.Insights/components', variables('webSiteName')))]": "Resource"
      },
      "properties": {
        "Name": "[variables('pingTestName')]",
        "Description": "Basic ping test",
        "Enabled": true,
        "Frequency": 300,
        "Timeout": 120,
        "Kind": "ping",
        "RetryEnabled": true,
        "Locations": [
          {
            "Id": "us-va-ash-azr"
          },
          {
            "Id": "emea-nl-ams-azr"
          },
          {
            "Id": "apac-jp-kaw-edge"
          }
        ],
        "Configuration": {
          "WebTest": "[concat('<WebTest   Name=\"', variables('pingTestName'), '\"   Enabled=\"True\"         CssProjectStructure=\"\"    CssIteration=\"\"  Timeout=\"120\"  WorkItemIds=\"\"         xmlns=\"http://microsoft.com/schemas/VisualStudio/TeamTest/2010\"         Description=\"\"  CredentialUserName=\"\"  CredentialPassword=\"\"         PreAuthenticate=\"True\"  Proxy=\"default\"  StopOnError=\"False\"         RecordedResultFile=\"\"  ResultsLocale=\"\">  <Items>  <Request Method=\"GET\"    Version=\"1.1\"  Url=\"', variables('pingURL'),   '\" ThinkTime=\"0\"  Timeout=\"300\" ParseDependentRequests=\"False\"         FollowRedirects=\"True\" RecordResult=\"True\" Cache=\"False\"         ResponseTimeGoal=\"0\"  Encoding=\"utf-8\"  ExpectedHttpStatusCode=\"200\"         ExpectedResponseUrl=\"\" ReportingName=\"\" IgnoreHttpStatusCode=\"False\" />        </Items>  <ValidationRules> <ValidationRule  Classname=\"Microsoft.VisualStudio.TestTools.WebTesting.Rules.ValidationRuleFindText, Microsoft.VisualStudio.QualityTools.WebTestFramework, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a\" DisplayName=\"Find Text\"         Description=\"Verifies the existence of the specified text in the response.\"         Level=\"High\"  ExecutionOrder=\"BeforeDependents\">  <RuleParameters>        <RuleParameter Name=\"FindText\" Value=\"',   parameters('pingText'), '\" />  <RuleParameter Name=\"IgnoreCase\" Value=\"False\" />  <RuleParameter Name=\"UseRegularExpression\" Value=\"False\" />  <RuleParameter Name=\"PassIfTextFound\" Value=\"True\" />  </RuleParameters> </ValidationRule>  </ValidationRules>  </WebTest>')]"
        },
        "SyntheticMonitorId": "[variables('pingTestName')]"
      }
      },
      {
        "type": "Microsoft.Insights/actionGroups",
        "apiVersion": "2018-03-01",
        "name": "[parameters('actionGroupName')]",
        "location": "Global",
        "properties": {
            "groupShortName": "[parameters('actionGroupShortName')]",
            "enabled": true,
            "emailReceivers": [
                {
                "name": "[parameters('emailName')]",
                "emailAddress": "[parameters('emailAddress')]"
                }
            ]
        }
      },
      {
        "name": "[variables('pingAlertRuleName')]",
        "type": "Microsoft.Insights/metricAlerts",
        "apiVersion": "2018-03-01",
        "location": "global",
        "dependsOn": [
          "[resourceId('Microsoft.Insights/webtests', variables('pingTestName'))]"
        ],
        "tags": {
          "[concat('hidden-link:', resourceId('Microsoft.Insights/components', variables('webSiteName')))]": "Resource",
          "[concat('hidden-link:', resourceId('Microsoft.Insights/webtests', variables('pingTestName')))]": "Resource"
        },
        "properties": {
          "description": "Alert for web test",
          "severity": 1,
          "enabled": true,
          "scopes": [
            "[resourceId('Microsoft.Insights/webtests',variables('pingTestName'))]",
            "[resourceId('Microsoft.Insights/components',variables('webSiteName'))]"
          ],
          "evaluationFrequency": "PT1M",
          "windowSize": "PT5M",
          "templateType": 0,
          "criteria": {
            "odata.type": "Microsoft.Azure.Monitor.WebtestLocationAvailabilityCriteria",
            "webTestId": "[resourceId('Microsoft.Insights/webtests', variables('pingTestName'))]",
            "componentId": "[resourceId('Microsoft.Insights/components', variables('webSiteName'))]",
            "failedLocationCount": 2
          },
          "actions": [
            {
              "actionGroupId": "[resourceId('Microsoft.Insights/actionGroups', parameters('actionGroupName'))]"
            }
          ]
        }
      },
      {
        "comments": "Service Health Activity Log Alert",
        "type": "microsoft.insights/activityLogAlerts",
        "name": "[parameters('activityLogAlertsName')]",
        "apiVersion": "2017-04-01",
        "location": "Global",
        "tags": {},
        "scale": null,
        "properties": {
            "scopes": [
              "[variables('alertScope')]"
            ],
            "condition": {
            "allOf": [
              {
                "field": "category",
                "equals": "ServiceHealth"
              },
              {
                "field": "properties.incidentType",
                "equals": "Incident"
              },
              {
                "field": "properties.impactedServices[*].ImpactedRegions[*].RegionName",
                "equals": null,
                "containsAny": "[parameters('ServiceHealthRegions')]"
              },
              {   
                "field": "properties.impactedServices[*].ServiceName",
                "equals": null,
                "containsAny": "[parameters('ServiceHealthServices')]"
              }
            ]
            },
            "actions": {
              "actionGroups": [
                {
                  "actionGroupId": "[resourceId('microsoft.insights/actionGroups', parameters('actionGroupName'))]",
                  "webhookProperties": {}
                }
              ]
            },
            "enabled": true,
            "description": ""
        },
        "dependsOn": [
          "[resourceId('microsoft.insights/actionGroups', parameters('actionGroupName'))]"
        ]
      }
    ]
}