# README #

This repo contains arm templates to deploy a basic app service web app in azure.  This will include an Azure SQL Server and database, and the web hosting.  An example app is included under the app directory.

### What is this repository for? ###

* Quick summary
* Version 1.0
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
    + Install Microsoft Azure CLI
    + Use az login to authenticate to azure
* Configuration
    + Edit the parameters file to change values like app name before deploy
    + The parameter for "leadImage" is used by the example app as the filename for the image to display
* Dependencies
    + Create your resource group in the Azure Portal prior to deploy.
    + if using the example app, have at least one image file to upload
* Database configuration
    + Since the database login and password are not supplied in the parameters file or the template, you will be prompted to enter those values when deploying.
* How to run tests
    + Validate the json template before deploying
     az deployment group validate -g <resource-group-name> --template-file paas-basic.json  --parameters @paas-basic.parameters.json
* Deployment instructions
    + deploy template
         az deployment group create -g <resource-group-name> --template-file paas-basic.json  --parameters @paas-basic.parameters.json
    + Using Azure portal, upload images to images container if using the example app
    + In the portal, for the SQL server firewall allow client IP (your IP) to connect
    + If using the example app, use query tool to create table and insert data  
    (change one of the .jpg to match your image name)
        - create table images (filename varchar(255),datetaken varchar(255),camera varchar(255),geolocation varchar(255));
        - INSERT INTO images (filename, datetaken, camera, geolocation) VALUES
        ('image1.jpg', '04-18-2019', 'Canon Powershot XLT', 'New York City, US'),
        ('image2.jpg', '12-20-1987', 'Sony Cybershot6', 'London, UK'),
        ('image3.jpg', '06-01-2002', 'Olympus Pen e-pl1', 'Toronto, CA');
    + under the staging slot for the app, configure a deployment center connection and push the app to it
    + use deployment slots to swap production with staging
    + enable azure ad auth in portal
