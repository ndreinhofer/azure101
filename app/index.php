
<?php

$leadImage = getenv("leadImage");
$appWelcomeTitle = getenv("appWelcomeTitle");
$imageStorageUrl = getenv("imageStorageUrl");
$dbName = getenv("dbName");
$dbUser = getenv("dbUser");
$dbServer = getenv("dbServer");
$dbPass = getenv("dbPass");

$query = "SELECT * FROM images WHERE filename = '$leadImage' ";

// SQL Server Extension Sample Code:
$connectionInfo = array("UID" => "$dbUser", "pwd" => "$dbPass", "Database" => "$dbName", "LoginTimeout" => 30, "Encrypt" => 1, "TrustServerCertificate" => 0);
$serverName = "$dbServer";
$conn = sqlsrv_connect($serverName, $connectionInfo)
    or die("Couldn't connect to SQL Server on $dbServer");

//execute the SQL query and return records
$result = sqlsrv_query($conn, $query);

// If the SQL query is succesfully performed ($result not false)
if($result !== false) {
    // Create the beginning of HTML table, and the first row with colums title
    $htmlTable = '<table border="1" cellspacing="0" cellpadding="2"><tr><th>Filename</th><th>Creation Date</th><th>Camera</th><th>Location</th></tr>';

    // Parse the result set, and adds each row and colums in HTML table

    while( $row = sqlsrv_fetch_array( $result, SQLSRV_FETCH_ASSOC) ) {
        $htmlTable .= '<tr><td>' . $row['filename'] . '</td><td>' . $row['datetaken'] . '</td><td>' . $row['camera'] . '</td><td>' . $row['geolocation'] . '</td></tr>';
    }
}
        
$htmlTable .= '</table>';           // ends the HTML table

echo "<html><body>" ;
echo "<h1>$appWelcomeTitle</h1>";
echo "<h2>Metadata for image:</h2>";
echo $htmlTable;        // display the HTML table
echo "<img src='$imageStorageUrl$leadImage' alt='Picture of a company product'>";
echo "</body></html>" ;
//close the connection
sqlsrv_close($conn);
?>
